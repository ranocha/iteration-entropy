# Install packages
using Pkg
Pkg.activate(@__DIR__)
Pkg.instantiate()


# Load packages
using DelimitedFiles
using LinearAlgebra
using SparseArrays

using ForwardDiff
using NLsolve

using SummationByPartsOperators
using Plots: Plots, plot, plot!, savefig


include("newton_krylov.jl")


# Physical setup of a traveling wave solution with speed `c()`
xmin() = -90.0
xmax() = -xmin()
c() = 1.2
function usol(t, x)
  A = 3 * (c() - 1)
  K = 0.5 * sqrt(1 - 1 / c())
  x_t = mod(x - c() * t - xmin(), xmax() - xmin()) + xmin()

  return A / cosh(K * x_t)^2
end


# Numerical setup of the semidiscretization with Fourier pseudospectral methods
function bbm_rhs_quadratic(u, param, t)
  (; D1, ImD2) = param
  one_third = one(t) / 3

  # this semidiscretization conserves the linear and quadratic invariants
  u_x = D1 * u
  return ImD2 \ (-one_third * (D1 * (u.^2)) - one_third .* u .* u_x .- u_x)
end

function bbm_rhs_cubic(u, param, t)
  (; D1_invImD2) = param

  # this semidiscretization conserves the linear and cubic invariants
  tmp = @. -(0.5 * u^2 + u)
  return D1_invImD2 * tmp
end

function bbm_setup(nnodes)
  D1 = fourier_derivative_operator(xmin(), xmax(), nnodes)
  D1op = D1
  D2 = D1^2
  ImD2 = I - D2
  D1_invImD2 = D1 / (I - D2)

  # tspan = (0.0, (xmax() - xmin()) / (3 * c()))
  # tspan = (0.0, (xmax() - xmin()) / (3 * c()) + 3 * (xmax() - xmin()) / c())
  tspan = (0.0, (xmax() - xmin()) / (3 * c()) + 10 * (xmax() - xmin()) / c())
  x = grid(D1)
  u0 = usol.(tspan[1], x)

  # FIXME?
  # Hacky conversion of the SBP operators to dense matrices to
  # allow simple calculation of the Jacobians via ForwardDiff.jl
  D1 = Matrix(D1)
  ImD2 = Matrix(ImD2)
  D1_invImD2 = Matrix(D1_invImD2)

  param = (; D1op, D1, D2, ImD2, D1_invImD2, usol)

  return u0, param, tspan
end


# Functionals to save during the time integration
function bbm_functionals(u, params, t)
  (; D2) = params
  D1 = params.D1op
  x = grid(D1)

  linear = integrate(u, D1)

  tmp1 = D2 * u
  @. tmp1 = u^2 - u * tmp1
  quadratic = integrate(tmp1, D1)

  @. tmp1 = (u + 1)^3
  cubic = integrate(tmp1, D1)

  @. tmp1 = u - usol(t, x)
  error_l2 = integrate(abs2, tmp1, D1) |> sqrt

  return (; linear, quadratic, cubic, error_l2)
end


# Relaxation overwriting `u` with the relaxed solution
function relax!(u, unew, params, rhs::typeof(bbm_rhs_quadratic))
  (; D2) = params
  D1 = params.D1op

  # For inner product norms, we have
  #   gamma = -2 * <u, unew - u> / |unew - u|^2
  diff = unew - u
  D2_diff = D2 * diff

  tmp = @. diff^2 - diff * D2_diff
  den = integrate(tmp, D1)

  @. tmp = u * (diff - D2_diff)
  num = integrate(tmp, D1)

  gamma = -2 * num / den
  @. u = u + gamma * (unew - u)

  return gamma
end

function relax!(u, unew, params, rhs::typeof(bbm_rhs_cubic))
  (; D2) = params
  D1 = params.D1op

  diff = unew - u
  tmp = similar(u)
  @. tmp = u * diff + 0.5 * u^2 * diff
  c = integrate(tmp, D1)

  @. tmp = 0.5 * diff^2 * (1 + u)
  b = integrate(tmp, D1)

  @. tmp = diff^3 / 6
  a = integrate(tmp, D1)

  s = sqrt(b^2 - 4 * a * c)
  gamma1 = (-b + s) / (2 * a)
  gamma2 = (-b - s) / (2 * a)
  gamma = gamma1
  if abs(gamma1 - 1) > abs(gamma2 - 1)
    gamma = gamma2
  end
  @. u = u + gamma * (unew - u)

  return gamma
end


# The main time integration loop
function bbm_solver(; rhs, alg,
                      relaxation = false,
                      nnodes = 2^6, dt = 0.25,
                      opts_newton = (abstol = 0.0, reltol = 1.0e-3, maxiter = 10),
                      opts_gmres = (etamax = 0.9, gamma = 0.9, maxiter = 200))
  u0, params, tspan = bbm_setup(nnodes)

  time      = Vector{Float64}()
  linear    = Vector{Float64}()
  quadratic = Vector{Float64}()
  cubic     = Vector{Float64}()
  error_l2  = Vector{Float64}()

  t = first(tspan)
  u = copy(u0)
  unew = similar(u)
  step = 0
  while t < last(tspan)
    if step % 100 == 0
      @info "running..." t step
    end
    step += 1

    if alg === :midpoint
      sol = nlsolve(u) do unew
        mid = @. 0.5 * (u + unew)
        return u .+ dt .* rhs(mid, params, t) .- unew
      end
      unew .= sol.zero
    elseif alg === :midpoint_newton
      f = unew -> begin
        mid = @. 0.5 * (u + unew)
        return u .+ dt .* rhs(mid, params, t) .- unew
      end
      J = unew -> ForwardDiff.jacobian(f, unew)
      unew .= u
      newton!(unew, f, J, opts_newton, opts_gmres)
    elseif alg === :avf
      sol = nlsolve(u) do unew
        mid = @. 0.5 * (u + unew)
        return u .+ (dt / 6) .* (rhs(u, params, t) .+
                                 4 .* rhs(mid, params, t) .+
                                 rhs(unew, params, t)) .- unew
      end
      unew .= sol.zero
    elseif alg === :avf_newton
      f = unew -> begin
        mid = @. 0.5 * (u + unew)
        return u .+ (dt / 6) .* (rhs(u, params, t) .+
                                 4 .* rhs(mid, params, t) .+
                                 rhs(unew, params, t)) .- unew
      end
      J = unew -> ForwardDiff.jacobian(f, unew)
      unew .= u
      newton!(unew, f, J, opts_newton, opts_gmres)
    elseif alg === :heun2
      k1 = rhs(u, params, t)
      y2 = @. u + dt * k1
      k2 = rhs(y2, params, t + dt)
      @. unew = u + (dt / 2) * (k1 + k2)
    elseif alg === :rk4
      k1 = rhs(u, params, t)
      y2 = @. u + 0.5 * dt * k1
      k2 = rhs(y2, params, t + 0.5 * dt)
      y3 = @. u + 0.5 * dt * k2
      k3 = rhs(y3, params, t + 0.5 * dt)
      y4 = @. u + dt * k3
      k4 = rhs(y4, params, t + dt)
      @. unew = u + (dt / 6) * (k1 + 2 * k2 + 2 * k3 + k4)
    end
    if relaxation
      gamma = relax!(u, unew, params, rhs)
      t += gamma * dt
    else
      t += dt
      u .= unew
    end

    functionals = bbm_functionals(u, params, t)
    push!(time,      t)
    push!(linear,    functionals.linear)
    push!(quadratic, functionals.quadratic)
    push!(cubic,     functionals.cubic)
    push!(error_l2,  functionals.error_l2)
  end

  x = grid(params.D1op)
  return (; time, linear, quadratic, cubic, error_l2, x, u, u0)
end


function plot_kwargs()
  fontsizes = (
    xtickfontsize = 14, ytickfontsize = 14,
    xguidefontsize = 16, yguidefontsize = 16,
    legendfontsize = 14)
  return (; linewidth = 4, gridlinewidth = 2, fontsizes...)
end

function bbm_main_quadratic()
  figdir = joinpath(dirname(@__DIR__), "Manuscript", "Pics")
  fig_error     = plot(xguide = "Time \$t\$", yguide = "\$L^2\$ error"; plot_kwargs()...)
  fig_quadratic = plot(xguide = "Time \$t\$", yguide = "Quadratic invariant"; plot_kwargs()...)
  fig_cubic     = plot(xguide = "Time \$t\$", yguide = "Cubic invariant"; plot_kwargs()...)

  res = bbm_solver(; nnodes = 2^6, dt = 0.25,
                     relaxation = false,
                     opts_newton = (abstol = 0.0, reltol = 1.0e-3, maxiter = 10),
                     rhs = bbm_rhs_quadratic, alg = :midpoint_newton)
  open(joinpath(figdir, "bbm_quadratic__tol_1em3.txt"), "w") do io
      println(io, "# t\tquadratic\tcubic\terror_l2")
      writedlm(io, hcat(res.time, res.quadratic, res.cubic, res.error_l2))
  end
  plot!(fig_error,     res.time, res.error_l2,  label = "Tolerance \$10^{-3}\$")
  plot!(fig_quadratic, res.time, res.quadratic, label = "Tolerance \$10^{-3}\$")
  plot!(fig_cubic,     res.time, res.cubic,     label = "Tolerance \$10^{-3}\$")

  res = bbm_solver(; nnodes = 2^6, dt = 0.25,
                     relaxation = false,
                     opts_newton = (abstol = 0.0, reltol = 1.0e-4, maxiter = 10),
                     rhs = bbm_rhs_quadratic, alg = :midpoint_newton)
  open(joinpath(figdir, "bbm_quadratic__tol_1em4.txt"), "w") do io
      println(io, "# t\tquadratic\tcubic\terror_l2")
      writedlm(io, hcat(res.time, res.quadratic, res.cubic, res.error_l2))
  end
  plot!(fig_error,     res.time, res.error_l2,  label = "Tolerance \$10^{-4}\$")
  plot!(fig_quadratic, res.time, res.quadratic, label = "Tolerance \$10^{-4}\$")
  plot!(fig_cubic,     res.time, res.cubic,     label = "Tolerance \$10^{-4}\$")

  res = bbm_solver(; nnodes = 2^6, dt = 0.25,
                     relaxation = true,
                     opts_newton = (abstol = 0.0, reltol = 1.0e-3, maxiter = 10),
                     rhs = bbm_rhs_quadratic, alg = :midpoint_newton)
  open(joinpath(figdir, "bbm_quadratic__tol_1em3_relaxation.txt"), "w") do io
      println(io, "# t\tquadratic\tcubic\terror_l2")
      writedlm(io, hcat(res.time, res.quadratic, res.cubic, res.error_l2))
  end
  plot!(fig_error,     res.time, res.error_l2,  label = "Tolerance \$10^{-3}\$, relaxation")
  plot!(fig_quadratic, res.time, res.quadratic, label = "Tolerance \$10^{-3}\$, relaxation")
  plot!(fig_cubic,     res.time, res.cubic,     label = "Tolerance \$10^{-3}\$, relaxation")

  return (; fig_error, fig_quadratic, fig_cubic)
end

function bbm_main_cubic()
  figdir = joinpath(dirname(@__DIR__), "Manuscript", "Pics")
  fig_error     = plot(xguide = "Time \$t\$", yguide = "\$L^2\$ error"; plot_kwargs()...)
  fig_quadratic = plot(xguide = "Time \$t\$", yguide = "Quadratic invariant"; plot_kwargs()...)
  fig_cubic     = plot(xguide = "Time \$t\$", yguide = "Cubic invariant"; plot_kwargs()...)

  res = bbm_solver(; nnodes = 2^6, dt = 0.25,
                     relaxation = false,
                     opts_newton = (abstol = 0.0, reltol = 1.0e-3, maxiter = 10),
                     rhs = bbm_rhs_cubic, alg = :avf_newton)
  open(joinpath(figdir, "bbm_cubic__tol_1em3.txt"), "w") do io
      println(io, "# t\tquadratic\tcubic\terror_l2")
      writedlm(io, hcat(res.time, res.quadratic, res.cubic, res.error_l2))
  end
  plot!(fig_error,     res.time, res.error_l2,  label = "Tolerance \$10^{-3}\$")
  plot!(fig_quadratic, res.time, res.quadratic, label = "Tolerance \$10^{-3}\$")
  plot!(fig_cubic,     res.time, res.cubic,     label = "Tolerance \$10^{-3}\$")

  res = bbm_solver(; nnodes = 2^6, dt = 0.25,
                     relaxation = false,
                     opts_newton = (abstol = 0.0, reltol = 1.0e-4, maxiter = 10),
                     rhs = bbm_rhs_cubic, alg = :avf_newton)
  open(joinpath(figdir, "bbm_cubic__tol_1em4.txt"), "w") do io
      println(io, "# t\tquadratic\tcubic\terror_l2")
      writedlm(io, hcat(res.time, res.quadratic, res.cubic, res.error_l2))
  end
  plot!(fig_error,     res.time, res.error_l2,  label = "Tolerance \$10^{-4}\$")
  plot!(fig_quadratic, res.time, res.quadratic, label = "Tolerance \$10^{-4}\$")
  plot!(fig_cubic,     res.time, res.cubic,     label = "Tolerance \$10^{-4}\$")

  res = bbm_solver(; nnodes = 2^6, dt = 0.25,
                     relaxation = true,
                     opts_newton = (abstol = 0.0, reltol = 1.0e-3, maxiter = 10),
                     rhs = bbm_rhs_cubic, alg = :avf_newton)
  open(joinpath(figdir, "bbm_cubic__tol_1em3_relaxation.txt"), "w") do io
      println(io, "# t\tquadratic\tcubic\terror_l2")
      writedlm(io, hcat(res.time, res.quadratic, res.cubic, res.error_l2))
  end
  plot!(fig_error,     res.time, res.error_l2,  label = "Tolerance \$10^{-3}\$, relaxation")
  plot!(fig_quadratic, res.time, res.quadratic, label = "Tolerance \$10^{-3}\$, relaxation")
  plot!(fig_cubic,     res.time, res.cubic,     label = "Tolerance \$10^{-3}\$, relaxation")

  return (; fig_error, fig_quadratic, fig_cubic)
end

function bbm_main()
  figdir = joinpath(dirname(@__DIR__), "Manuscript", "Pics")

  let figs = bbm_main_quadratic()
    savefig(figs.fig_error, joinpath(figdir, "bbm_quadratic__error.pdf"))
  end

  let figs = bbm_main_cubic()
    savefig(figs.fig_error, joinpath(figdir, "bbm_cubic__error.pdf"))
  end

  @info "Results saved in the directory `figdir`" figdir

  return nothing
end

# res = bbm_solver(; nnodes = 2^6, dt = 0.25, rhs = bbm_rhs_quadratic, alg = :midpoint); plot(res.time, res.error_l2, label = "quadratic")
# res = bbm_solver(; nnodes = 2^6, dt = 0.25, rhs = bbm_rhs_quadratic, alg = :midpoint_newton, opts_newton = (abstol = 0.0, reltol = 1.0e-3, maxiter = 10)); plot(res.time, res.error_l2, label = "quadratic, Newton (1.0e-3)")
# res = bbm_solver(; nnodes = 2^6, dt = 0.25, rhs = bbm_rhs_quadratic, alg = :midpoint_newton, opts_newton = (abstol = 0.0, reltol = 1.0e-4, maxiter = 10)); plot(res.time, res.error_l2, label = "quadratic, Newton (1.0e-4)")

# res = bbm_solver(; nnodes = 2^6, dt = 0.25, rhs = bbm_rhs_cubic, alg = :avf); plot(res.time, res.error_l2, label = "cubic")
# res = bbm_solver(; nnodes = 2^6, dt = 0.25, rhs = bbm_rhs_cubic, alg = :avf_newton, opts_newton = (abstol = 0.0, reltol = 1.0e-3, maxiter = 10)); plot(res.time, res.error_l2, label = "cubic, Newton (1.0e-3)")
# res = bbm_solver(; nnodes = 2^6, dt = 0.25, rhs = bbm_rhs_cubic, alg = :avf_newton, opts_newton = (abstol = 0.0, reltol = 1.0e-4, maxiter = 10)); plot(res.time, res.error_l2, label = "cubic, Newton (1.0e-4)")

# res = bbm_solver(; nnodes = 2^6, dt = 0.25, rhs = bbm_rhs_cubic, alg = :heun2); plot(res.time, res.error_l2, label = "explicit")

# plot(res.x, res.u, label = "u"); plot!(res.x, res.u0, label = "u0")