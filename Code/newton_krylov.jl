using Krylov: Krylov

# Newton's method with direct linear solves
function newton!(x,f,J,NewtonOpts,gmresOpts::String)

    # Unpack parameters for the iterative solver
    abs_tol_N = NewtonOpts.abstol
    rel_tol_N = NewtonOpts.reltol
    max_N = NewtonOpts.maxiter

    # For book keeping
    Nx = length(x)
    it_count = 0
    res_hist = zeros(max_N+1)

    # Used only for line search
    un = copy(x)

    # Evaluate function and norm at initial iterate
    F_val = f(x)
    F_val_norm = norm(F_val)/sqrt(Nx)
    res_hist[1] = F_val_norm
    stop_tol = abs_tol_N + rel_tol_N*F_val_norm

    # Solve nonlinear system using Newton
    for ns = 1:max_N
        if F_val_norm < stop_tol
            break
        end

        # Direct linear solve
        x .-= J(x)\F_val

        # Line search
        #dx = -J(x)\F_val
        #y = x + dx
        #a = -(dx'*x + y'*(x-un))/(dx'*dx)
        #x .= a*y + (1-a)*x

        F_val = f(x)
        F_val_norm = norm(F_val)/sqrt(Nx)
        res_hist[ns+1] = F_val_norm
        it_count = ns
    end

    if it_count == max_N
        @warn "Maximum number of Newton iterations reached"
    end

    res_hist = res_hist[1:it_count+1]
    return (x, res_hist)

end


# Newton's method with gmres as linear solver
function newton!(x,f,J,NewtonOpts,gmresOpts)

    # Unpack parameters for the iterative solver
    abs_tol_N = NewtonOpts.abstol
    rel_tol_N = NewtonOpts.reltol
    max_N = NewtonOpts.maxiter

    tolmax = gmresOpts.etamax
    max_G = gmresOpts.maxiter
    gamma = gmresOpts.gamma

    # For book keeping
    Nx = length(x)
    it_count = 0
    res_hist = zeros(max_N+1)

    # Evaluate function and norm at initial iterate
    f_val = f(x)
    f_val_norm = norm(f_val)/sqrt(Nx)
    res_hist[1] = f_val_norm
    stop_tol = abs_tol_N + rel_tol_N*f_val_norm

    tol_G = gmresOpts.etamax

    # Set up a GMRES solver object containing some caches
    gmres_solver = Krylov.GmresSolver(J(x), f_val)

    # Solve nonlinear system using Newton
    for ns = 1:max_N
        if f_val_norm < stop_tol
            break
        end

        # Linear solve
        f_val_norm_old = f_val_norm
        Krylov.gmres!(gmres_solver, J(x), f_val; rtol = tol_G, itmax = max_G, restart = false)
        x .-= Krylov.solution(gmres_solver)

        if Krylov.statistics(gmres_solver) == max_G
            @warn "Maximum number of GMRES iterations reached"
        end

        # Update tolerance for gmres
        f_val = f(x)
        f_val_norm = norm(f_val)/sqrt(Nx)
        res_hist[ns+1] = f_val_norm
        it_count = ns
        tol_G = EisenstatWalker!(tol_G,f_val_norm,f_val_norm_old,gamma,tolmax,stop_tol)
    end

    if it_count == max_N
        @warn "Maximum number of Newton iterations reached"
    end

    res_hist = res_hist[1:it_count+1]
    return (x,res_hist)

end



function EisenstatWalker!(eta,fnorm_new,fnorm_old,gamma,etamax,stopTol)
    # Choose gmres forcing parameter using Eisenstat-Walker

    rat = fnorm_new / fnorm_old

    eta2 = eta*eta;
    etanew = gamma*rat*rat;
    if gamma*eta2 > 0.1
        etanew = max(etanew,gamma*eta2);
    end
    etanew = min(etanew,etamax);
    eta = max(etanew,0.5*stopTol/fnorm_new);
    return eta
end